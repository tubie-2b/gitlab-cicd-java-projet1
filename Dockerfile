# Utilisez l'image OpenJDK 17 comme base
FROM openjdk:17-alpine

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez le JAR de l'application dans le conteneur
COPY build/libs/*.jar app.jar

# Exposez le port sur lequel votre application s'exécute
EXPOSE 8070

# Commande pour exécuter votre application Spring Boot lorsque le conteneur démarre
CMD ["java", "-jar", "app.jar"]
