package com.example.demo.services_impl;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.demo.dto.MessageDto;
import com.example.demo.models.Message;
import com.example.demo.repositories.MessageRepository;


public class MessageServiceImplTests {

    @Mock
    private MessageRepository messageRepository;

    @InjectMocks
    private MessageServiceImpl messageService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @SuppressWarnings("null")
    @Test
    public void testCreateMessage() {
        // Préparation des données de test
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Message de test");

        // Exécution de la méthode à tester
        messageService.create(messageDto);

        // Vérification que la méthode save du repository a été appelée une fois avec n'importe quel argument de type Message
        verify(messageRepository, times(1)).save(any(Message.class));

        // Vérification que la méthode setCreateAt a été appelée une fois sur l'objet Message passé à la méthode save
        verify(messageRepository, times(1)).save(argThat(message -> {
            return message.getCreateAt() != null; // Vérifie que la date de création a été définie
        }));
    }

    @Test
    public void testGetAllMessages() {
        // Préparez les données de test
        List<Message> expectedMessages = new ArrayList<>();
        // Ajoutez quelques messages fictifs à la liste
        expectedMessages.add(new Message());
        expectedMessages.add(new Message());

        // Configurez le comportement du service
        when(messageService.all()).thenReturn(expectedMessages);

        // Appelez la méthode à tester
        List<Message> result = messageService.all();

        // Vérifiez que la méthode du service a été appelée
        verify(messageRepository).findAll();

        // Vérifiez le contenu de la réponse
        assertEquals(expectedMessages, result);
    }

}
