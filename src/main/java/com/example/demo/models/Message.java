package com.example.demo.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity(name = "message")
@SuppressWarnings("unused")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private String id;
	private String text;
	private String createAt;
	private String updateAt; 
}
