package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.lang.NonNull;

import com.example.demo.dto.MessageDto;
import com.example.demo.models.Message;

public interface MessageServices {
	void create(@NonNull MessageDto newMessage);
	List<Message> all();
	Optional<Message> one(@NonNull String id);
	Message update(@NonNull String id, @NonNull MessageDto newMessage);
	void delete(@NonNull String id);
}
