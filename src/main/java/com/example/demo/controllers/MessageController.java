package com.example.demo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MessageDto;
import com.example.demo.models.Message;
import com.example.demo.services_impl.MessageServiceImpl;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v0/messages")
public class MessageController {
    private final MessageServiceImpl messageService;

    public MessageController(MessageServiceImpl messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/create")
    public ResponseEntity<String> createMessage(@RequestBody @NonNull MessageDto message) {
        this.messageService.create(message);
        return new ResponseEntity<>("Message created successfully", HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Message>> getAllMessages() {
        List<Message> messages = this.messageService.all();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }
}
