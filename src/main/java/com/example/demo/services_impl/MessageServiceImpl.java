package com.example.demo.services_impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MessageDto;
import com.example.demo.models.Message;
import com.example.demo.repositories.MessageRepository;
import com.example.demo.services.MessageServices;

@Service
public class MessageServiceImpl implements MessageServices {

	private MessageRepository messageRepository;

	public MessageServiceImpl(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	@Override
	public void create(@NonNull MessageDto message) {
		String id = UUID.randomUUID().toString();
		Message newMessage = new Message();

		newMessage.setId(id);
		newMessage.setText(message.getText());

		String dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a")
				.format(LocalDateTime.now());

		newMessage.setCreateAt(dateTime);
		newMessage.setUpdateAt(dateTime);

		this.messageRepository.save(newMessage);
	}

	@Override
	public List<Message> all() {
		return this.messageRepository.findAll();
	}

	@Override
	public Optional<Message> one(@NonNull String id) {
		return this.messageRepository.findById(id);
	}

	@Override
	public Message update(@NonNull String id, @NonNull MessageDto newMessage) {
		Message existingTask = this.messageRepository.getReferenceById(id);

		if (existingTask.getText() != null && !existingTask.getText().isEmpty()) {
			existingTask.setText(newMessage.getText());
		}

		String dateTime = DateTimeFormatter.ofPattern("MMM dd yyyy, hh:mm:ss a")
				.format(LocalDateTime.now());

		existingTask.setUpdateAt(dateTime);
		this.messageRepository.save(existingTask);

		return existingTask;

	}

	@Override
	public void delete(@NonNull String id) {
		this.messageRepository.deleteById(id);
	}

}
